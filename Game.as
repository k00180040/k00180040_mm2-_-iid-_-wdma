﻿package 
{
	import interfaces.IState;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	
	import states.Menu;
	import states.Play;
	import states.Play2;
	import states.Play3;
	import states.Instructions;	
	
	
	public class Game extends Sprite
	{
		public static const MENU_STATE:int = 0;
		public static const PLAY_STATE:int = 1;
		public static const INSTRUCTIONS_STATE:int = 2;
		public static const PLAY2_STATE:int = 4;
		public static const PLAY3_STATE:int = 5;
	
		
		private var current_state:IState;
		
		public function Game()
		{
	
			addEventListener(Event.ADDED_TO_STAGE, init);

		}
		
		private function init(event:Event):void
		{
			
			
			changeState(MENU_STATE);
		
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		public function changeState(state:int):void
		{
				
			if(current_state != null)
			{
				
				current_state = null;
			}
			
			switch(state)
			{
				case MENU_STATE:
					
					current_state = new Menu(this);
					break;
				
				case PLAY_STATE:
					current_state = new Play(this);
					break;
				
				case PLAY2_STATE:
					current_state = new Play2(this);
					break;
				
				case PLAY3_STATE:
					current_state = new Play3(this);
					break;
				
				case INSTRUCTIONS_STATE:
					
					current_state = new Instructions(this);
				
					break;
				
			
			
			}
			
			addChild(Sprite(current_state));
			
		}
		
		private function update(event:Event):void
		{
			var Screen2Tween:Tween = new Tween(current_state, "x", Back.easeInOut, 0, 1980, 1.5, true);
			current_state.update();
		}
	}
}