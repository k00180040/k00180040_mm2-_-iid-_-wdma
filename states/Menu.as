﻿package states
{
	import Game;
	
	import interfaces.IState;
	
	import objects.Background;
	import objects.Title;
	import objects.Girl;
	import objects.Intro;
	import objects.Play_pencil;
	import objects.How_pencil;
	
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import flash.display.SimpleButton;
	import flash.text.Font;
	import flash.text.TextFormat;	
	import flash.display.StageScaleMode;
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	import flash.media.Sound;
	

	/* all sound effects taken from - http://www.freesfx.co.uk*/

	
	public class Menu extends Sprite implements IState
	{
		private var game:Game;
		private var background:Background;
	    private var title:Title;
		private var topImage:Girl;
		private var intro:Intro;
	    private var chaklSound:Sound=new chalk();
		var clickButton:Sound=new click();
		
		public function Menu(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
			
			chaklSound.play(12000);
		}
		
		
		private function init(event:Event):void
		{
			background = new Background();
			//addChild(background);
			
			

			
			var topImage=new Girl();
			topImage.x=0;
			topImage.y=0;
			
			addChild(topImage);
			stage.scaleMode = StageScaleMode.EXACT_FIT;
			
			var intro=new Intro();
			intro.x=100;
			intro.y=100;
			intro.width=800;
			intro.height=600;
			addChildAt(intro,0);

								
			

			var playButton:Play_pencil=new Play_pencil;			
			addChild(playButton); 
	
			playButton.toggle =true; 
			playButton.width=300;
			playButton.height=180;
			playButton.x=400;
			playButton.y=880;
			playButton.addEventListener(MouseEvent.CLICK, onPlay); 
			
			var instructionsButton:How_pencil = new How_pencil; 
			addChild(instructionsButton); 
	
			
			instructionsButton.toggle =true; 
			instructionsButton.width=300;
			instructionsButton.height=180;
			instructionsButton.x=900
			instructionsButton.y=880;
			instructionsButton.addEventListener(MouseEvent.CLICK, onInstructions); 
			
	
					
	
		}
		
		private function onPlay(event:Event):void
		{
			clickButton.play();
			game.changeState(Game.PLAY_STATE);
		}
		
		private function onInstructions(event:Event):void
		{
			clickButton.play();
			game.changeState(Game.INSTRUCTIONS_STATE);
		}
		
		public function update():void
		{
			background.update();
		}
		
		public function destroy():void
		{
			//background.removeFromParent();

			background = null;
			
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child)
		}}}