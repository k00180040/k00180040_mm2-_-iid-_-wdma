﻿package states
{
	import Game;
	
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.media.Sound;
	import flash.system.fscommand;
	import flash.display.StageScaleMode;
	import flash.ui.Multitouch;
	import flash.sampler.NewObjectSample;
	import flash.text.TextFormat;	
	import objects.*;	
	import interfaces.IState;
	import fl.controls.CheckBox;

	
	
	public class Play2 extends Sprite implements IState
	{
		private var background:Background;
		public var mathGame:Game;
		
		
		var div:Div=new Div;
		var divbr:DivBr=new DivBr;
		var lbr:LeftBr=new LeftBr;
		var rbr:RightBr=new RightBr;
		var min:Minus=new Minus;
		var minbr:MinusBr=new MinusBr;
		var plus:Plus=new Plus;
		var mult1:Mult=new Mult;
		var mult2:Mult=new Mult;
		var mbr:MultBr=new MultBr;
		var equals:Equals=new Equals;
		var mult1_clicked=new Mult_Clicked;
		var minusBr_clicked=new MinusBr_Clicked;
		
		
		var zero:Zero=new Zero;
		var one:One=new One;
		var two:Two = new Two;
		var three:Three=new Three;
		var four:Four=new Four;
		var five:Five=new Five;
		var six:Six=new Six;
		var seven:Seven=new Seven;
		var eight:Eight=new Eight;
		var nine:Nine=new Nine;
		
		var cleanBoard:CleanBoard;//background for the result of the game to display
		var blueSheet:BlueSheet=new BlueSheet;
		public var sample1:Array=new Array;
		public var sample2:Array=new Array;
		public var sample3:Array=new Array;
		public var sample4:Array=new Array;
		public var letters:Array=new Array;//BODMAS letters
		
		var sample1_image:Sample1_im=new Sample1_im;//images to display sorted steps
		var sample2_image:Sample2_im=new Sample2_im;
		var sample3_image:Sample3_im=new Sample3_im;
		
		var good:Good=new Good;   //well done hint_msg at the end of the example 1		
		var clicks:int=new int; //count clicks
		var status_img:Girl_back=new Girl_back;
		var sad_girl:sad=new sad;
		var check_mark:check=new check;
		
	
		var hint_msg:TextField = new TextField();
		var title:Title = new Title();
		var clicks_done:TextField=new TextField();//set the hint_msg about clicks done 
	
		var childBAdded:Boolean=new Boolean;
		var childOAdded:Boolean=new Boolean;
		var childDAdded:Boolean=new Boolean;
		var childAAdded:Boolean=new Boolean;
		var childhint_msgAdded:Boolean=new Boolean;
		var childMult_clicked:Boolean=new Boolean;
		var childMinBr_clicked:Boolean=new Boolean;
		var sad_girlAdded:Boolean=new Boolean;
		
		
		var B:Bodmas_B=new Bodmas_B;
		var O:Bodmas_O=new Bodmas_O;
		var D:Bodmas_D=new Bodmas_D;		
		var A:Bodmas_A=new Bodmas_A;
		
		var noSound:Sound=new no();
		var clapSound:Sound=new clap();
		var clickButton:Sound=new click();
		
		public function Play2(mathGame:Game)
		{
			this.mathGame = mathGame;
				
			addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.COMPLETE, onGameOver);
			screen1();
			addLetters();//adds BOMDAS coordinates , as they will be same for all
			
			
		

		}
		
		private function addLetters():void{
			letters=[B,O,D,A];
			for(var i:int=0;i<letters.length;i++){
				letters[i].x=1180;
				letters[i].y=416;
			}
		}
	
		
		private function screen1():void{
		
			
			addChild(title);

			
			
			
			
			var hintFormat:TextFormat = new TextFormat();
			hintFormat.font="Mistral";
			hintFormat.size = 30;
			hintFormat.bold=true;
			

			
			 hint_msg.defaultTextFormat = hintFormat;
			 hint_msg.width = 500;
			 hint_msg.height = 600;
			 hint_msg.x = 1400;
			 hint_msg.y = 450;
			 hint_msg.textColor=0xFFFFFF;
			 
			 var clicksFormat:TextFormat=new TextFormat();
			 clicksFormat.font="Mistral";
			 clicksFormat.size=60;
			
			 clicks_done.defaultTextFormat=clicksFormat;
			
			 clicks_done.x=180;
			 clicks_done.y=720;
			 clicks_done.width=1200;
			 clicks_done.height=400;
			 clicks_done.textColor=0xFFFFFF;
			 
			 
			 sad_girl.x=1420;
			 sad_girl.y=580;
		
			
			
	
		
			//trace("x"+stage.mouseX+" "+stage.mouseY);
		
			// setting and displaying 3X(10-4X2)
			sample1=[three,mult1,lbr,one,zero,minbr,four,mbr,two,rbr];
			
			
			for(var i:int=0;i<sample1.length;i++){
				sample1[i].x=180+i*60;
				sample1[i].y=230;
				addChild(sample1[i]);
				
				sample1[i].addEventListener( MouseEvent.CLICK, click1 );
				
				

			}
		}//screen1 ends
		
		//3X(10-4x2)
		
		function click1(event:MouseEvent):void{
			sad_girlAdded=false;
			
			switch(event.currentTarget){
				
				case mult1:
					noSound.play();
					clicks++;
				
					mult1_clicked.x=220;
					mult1_clicked.y=230;
					removeChild(mult1);					
					addChild(mult1_clicked);
					childMult_clicked=true;
				
					hint_msg.text="DO OPERATORS \n\nIN BRACKETS FIRST";
					addChild(hint_msg);
					addChild(B);
					addChild(title);
					childBAdded=true;
				
					if(sad_girlAdded==false){
						addChild(sad_girl);
						sad_girlAdded=true;
					}
										
				
					if(childDAdded==true){
					removeChild(D);
					childDAdded=false;	
					
						}
					break;
				
					//if minus inside brackets clicked
					case minbr:
					noSound.play();
					clicks++;
					minusBr_clicked.x=480;
					minusBr_clicked.y=230;
					removeChild(minbr);
					addChild(minusBr_clicked);
					addChild(title);
					childMinBr_clicked=true;
					
					if(sad_girlAdded==false){
						addChild(sad_girl);
						sad_girlAdded=true;
					}
					
				
					hint_msg.text="YOU ARE IN BRACKETS -\n\nRIGHT STEP\n\nBUT DO MULTIPLICATION / DIVISION  \n\nFIRST";
					addChild(hint_msg);
					addChild(D);
					childDAdded=true;
					childhint_msgAdded=true;
				
					if(childBAdded==true){
					removeChild(B);
					childBAdded=false;
						}
					break;
				
				case mbr:
					//if(childMult_clicked=true){
						//removeChild(mult1_clicked);
					//}
					//if(childMinBr_clicked=true){
						//removeChild(minusBr_clicked);
					//}
				
					
					for(var j:int=0;j<sample1.length;j++){
						removeChildAt(0);
						sample1[j].removeEventListener( MouseEvent.CLICK, click1 );
						}				
					
					sample2=[equals,three,mult1,lbr,one,zero,minbr,eight,rbr];	
					for(var i:int=0;i<sample2.length;i++){
					sample2[i].x=120+i*60;
					sample2[i].y=330;
					addChild(sample2[i]);
					sample2[i].addEventListener( MouseEvent.CLICK, click2 );
					
							}
					sample1_image.x=188;
					sample1_image.y=240;
					addChild(sample1_image);
					addChild(title);
					clicks++;
							
				
							
					if(childBAdded==true ){
					removeChild(B);
					removeChild(hint_msg);
						removeChild(sad_girl);
							}				
										
					if(childDAdded==true ){
					removeChild(D);
					removeChild(hint_msg);
					removeChild(sad_girl);
							}
							
					//if(childMinBr_clicked=true){
						//removeChild(minusBr_clicked);
					//}
					check_mark.x=980;
					check_mark.y=220;
					addChild(check_mark);
			
						
					
					}
					
				}
				
				//3x(10-8)
				
				function click2(event:MouseEvent):void{
					if(sad_girlAdded==true){
						removeChild(sad_girl);
					}
					
				switch(event.currentTarget){
					
				case mult1:
					noSound.play();
					clicks++;
					mult1_clicked.x=228;
					mult1_clicked.y=330;
					addChildAt(mult1_clicked,0);
					removeChild(mult1);
					hint_msg.text="DO OPERATORS \n\nIN BRACKETS FIRST";
					addChild(hint_msg);
					addChild(B);
					addChild(title);
					childBAdded=true;
					addChild(sad_girl);
					break;				
				
					case minbr:	
						
					
					
						
					for(var j:int=0;j<sample2.length;j++){
					removeChildAt(0);
					sample2[j].removeEventListener( MouseEvent.CLICK, click2 );
							
						}			
			
					sample3=[equals,three,mult1,two];	
					for(var i:int=0;i<sample3.length;i++){
					sample3[i].x=120+i*60;
					sample3[i].y=424;
					addChild(sample3[i]);
					sample3[i].addEventListener( MouseEvent.CLICK, click3 );
					
							}
					sample2_image.x=268;
					sample2_image.y=356;
					addChild(sample2_image);
					clicks++;
							
					check_mark.y=320;
					addChild(check_mark);
					addChild(title);
							
						if(childBAdded==true){
					removeChild(B);
					removeChild(hint_msg);
					removeChild(sad_girl);
						}
					break;
						
						
						}
						
						
					}
					
					function click3(event:MouseEvent):void{
				
					clicks++;
					clapSound.play();
						
					sample3_image.x=144;
					sample3_image.y=454;
					addChild(sample3_image);
					addChild(sample1_image);
					addChild(title);
					check_mark.y=420;
					addChild(check_mark);
						
					good.x=900;
					good.y=230;
					addChild(good);
					
	
				
						
							
						
						
						if(clicks>3){
							
							clicks_done.text="YOU DID IT IN "+clicks+" CLICKS\t TRY TO DO IT IN 3";
							}
							else{
							clicks_done.text="YOU DID IT WITHOUT MISTAKES!";
							}
							addChild(clicks_done);
						
						
						for(var j:int=0;j<sample3.length;j++){
						removeChildAt(0);
						sample3[j].removeEventListener( MouseEvent.CLICK, click3 );
						}	
						removeChild(check_mark);
						
						addButtons();
							
					}
					private function addButtons():void{
					
						
						var playButton:More_pencil= new More_pencil;
						
						addChild(playButton); 
					
						
						playButton.toggle =true; 
						playButton.width=320;
						playButton.height=200;
						playButton.x=240;
						playButton.y=900;
						playButton.addEventListener(MouseEvent.CLICK, onPlay_screen3); 
						
						var exitButton:Exit_pencil = new Exit_pencil; 
						addChild(exitButton); 
					
						
						exitButton.toggle =true; 
						exitButton.width=320;
						exitButton.height=200;
						exitButton.x=880;
						exitButton.y=900;
						exitButton.addEventListener(MouseEvent.CLICK, exit); 
						
				}
				
					private function onPlay_screen3(event:Event):void
				{
					clickButton.play();
					mathGame.changeState(Game.PLAY3_STATE);
				}
					private function exit(event:Event):void
				{
					clickButton.play();					
					fscommand("quit");
				}
							
							
							
						
					
			
		private function init(event:Event):void
		{
			stage.scaleMode = StageScaleMode.EXACT_FIT;
			background = new Background(2);
			stage.addChildAt(background,0);
			
			
					
						}
		
		
				
		private function onGameOver(event:Event):void
		{
		
			//mathGame.changeState(Game.GAME_OVER_STATE);
			
		}
		
				
		
		private function onMenu(event:Event):void
		{
			mathGame.changeState(Game.MENU_STATE);
		}
		public function update():void
		{
			background.update();
			
		}
		
		public function destroy():void
		{
		
			removeEventListener(Event.COMPLETE, onGameOver);
			//removeEventListener(Event.ENTER_FRAME,loopGame);			
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}
