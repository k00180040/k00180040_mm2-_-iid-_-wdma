﻿package states
{
	import Game;
	
	import interfaces.IState;	
	import objects.Background;
	import objects.Title;
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import flash.system.fscommand;
	
	

	
	public class Win extends Sprite implements IState
	{
		private var game:Game;
		private var background:Background;		
		private var wonImage:won;
		
	
		
		public function Win(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			background = new Background();
			addChild(background);
			
					
			
			wonImage=new won();
			wonImage.x=260;
			wonImage.y=260;
			addChild(wonImage);
 			
			
			var playButton:Button = new Button(); 
			addChild(playButton); 
			playButton.label = "PLAY AGAIN?"; 
			playButton.toggle =true; 
			playButton.width=120;
			playButton.height=40;
			playButton.move(80, 720);			
			playButton.addEventListener(MouseEvent.CLICK, onPlay); 
			
			var exitButton:Button = new Button(); 
			addChild(exitButton); 
			exitButton.label = "EXIT"; 
			exitButton.toggle =true; 
			exitButton.width=120;
			exitButton.height=40;
			exitButton.move(300, 720);
			exitButton.addEventListener(MouseEvent.CLICK, exit); 
			
			
			}
		
		  
		private function exit(event:MouseEvent):void {  
			fscommand("quit");  
			
		}  

		private function onPlay(event:Event):void
		{
			game.changeState(Game.PLAY_STATE);
		}
		
		private function onInstructions(event:Event):void
		{
			game.changeState(Game.INSTRUCTIONS_STATE);
		}
		
		private function onMenu(event:Event):void
		{
			game.changeState(Game.MENU_STATE);
		}
			
		
		private function onAgain(event:Event):void
		{
		
			game.changeState(Game.PLAY_STATE);
		}
		
		public function update():void
		{
			background.update();
		}
		
		public function destroy():void
		{
			removeFromParent();
		}
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}