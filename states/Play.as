﻿
package states
{
	import Game;
	
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import fl.controls.Button;
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.media.Sound;
	import flash.system.fscommand;
	import flash.display.StageScaleMode;
	import flash.ui.Multitouch;
	import flash.sampler.NewObjectSample;
	import flash.text.TextFormat;	
	import fl.controls.CheckBox;
	
	import objects.*;	
	import interfaces.IState;

	/* all sound effects taken from - http://www.freesfx.co.uk*/
	
	public class Play extends Sprite implements IState
	{
		
		
		private var background:Background;
		public var mathGame:Game;
		
		
		var div:Div=new Div;	
		var min:Minus=new Minus;	
		var plus:Plus=new Plus;		
		var equals:Equals=new Equals;
		var plusSign_clicked:Plus_clicked=new Plus_clicked;
		var min_clicked:Min_clicked=new Min_clicked;
		
		
		var zero:Zero=new Zero;
		var one:One=new One;
		var two:Two = new Two;
		var three:Three=new Three;
		var four:Four=new Four;
		var five:Five=new Five;
		var six:Six=new Six;
		var seven:Seven=new Seven;
		var eight:Eight=new Eight;
		var nine:Nine=new Nine;
		
		var cleanBoard:CleanBoard;//background for the result of the game to display
		
		public var sample1:Array=new Array;
		public var sample2:Array=new Array;
		public var sample3:Array=new Array;
		public var sample4:Array=new Array;
		public var letters:Array=new Array;//BODMAS letters
		
		var sample1_image:Sample4_im=new Sample4_im;//images to display sorted steps
		var sample2_image:Sample5_im=new Sample5_im;
		var sample3_image:Sample6_im=new Sample6_im;
		
		var well:Well=new Well;   //well done hint_msg at the end of the example 1		
		var clicks:int=new int; //count clicks
		var status_img:Girl_back=new Girl_back;
		var sad_girl:sad=new sad;
		var check_mark:check=new check;
		
	
		var hint_msg:TextField = new TextField();
		var title:Title = new Title();
		var clicks_done:TextField=new TextField();//set the hint_msg about clicks done 
	
		var childBAdded:Boolean=new Boolean;
		var childOAdded:Boolean=new Boolean;
		var childDAdded:Boolean=new Boolean;
		var childAAdded:Boolean=new Boolean;
		var childhint_msgAdded:Boolean=new Boolean;
		var childPlus_clicked:Boolean=new Boolean;
		var childMin_clicked:Boolean=new Boolean;
		var sad_girlAdded:Boolean=new Boolean;
		
		
		var B:Bodmas_B=new Bodmas_B;
		var O:Bodmas_O=new Bodmas_O;
		var D:Bodmas_D=new Bodmas_D;		
		var A:Bodmas_A=new Bodmas_A;
		
		var noSound:Sound=new no();
		var clapSound:Sound=new clap();
		var clickButton:Sound=new click();

		
		public function Play(mathGame:Game)
		{
			this.mathGame = mathGame;
			//Assets.init();
			addEventListener(Event.ADDED_TO_STAGE, init);
		
			screen1();
			addLetters();//adds BOMDAS coordinates , as they will be same for all
			
			
		

		}
		
		private function addLetters():void{
			letters=[B,O,D,A];
			for(var i:int=0;i<letters.length;i++){
				letters[i].x=1180;
				letters[i].y=416;
			}
		}
	
		
		private function screen1():void{
			
			
			
			addChild(title);
		
		
			
		var hintFormat:TextFormat = new TextFormat();
		hintFormat.font="Mistral";
		hintFormat.size = 30;
		hintFormat.bold=true;
			

			
			 hint_msg.defaultTextFormat =hintFormat;
			 hint_msg.width = 600;
			 hint_msg.height = 600;
			 hint_msg.x = 1400;
			 hint_msg.y = 450;
			 hint_msg.textColor=0xFFFFFF;
			
			 var clicksFormat:TextFormat=new TextFormat();
			 clicksFormat.font="Mistral";
			 clicksFormat.size=60;
			
			 clicks_done.defaultTextFormat=clicksFormat;
			
			 clicks_done.x=180;
			 clicks_done.y=720;
			 clicks_done.width=1200;
			 clicks_done.height=400;
			 clicks_done.textColor=0xFFFFFF;
			 
			 sad_girl.x=1420;
			 sad_girl.y=580;
			
		
		
			// setting and displaying 6+4/2-7
			sample1=[six,plus,four,div,two,min,seven];
			
			
			for(var i:int=0;i<sample1.length;i++){
				sample1[i].x=180+i*60;
				sample1[i].y=240;
				addChild(sample1[i]);
				
				sample1[i].addEventListener( MouseEvent.CLICK, click1 );
				
				

			}
		}//screen1 ends
		
		//6+4/2-7
		
		function click1(event:MouseEvent):void{
			sad_girlAdded=false;
			
			switch(event.currentTarget){
				
				case plus:
					noSound.play();
					clicks++;
				
					plusSign_clicked.x=238;
					plusSign_clicked.y=242;
					removeChild(plus);					
					addChild(plusSign_clicked);
					childPlus_clicked=true;
				
					hint_msg.text="DO MULTIPLICATION / DIVISION FIRST";
					addChild(hint_msg);
					addChild(D);
					addChild(title);
					childDAdded=true;
				
					if(sad_girlAdded==false){
						addChild(sad_girl);
						sad_girlAdded=true;
					}
					break;
						
					case min:
					noSound.play();
					clicks++;
					if(childDAdded==true){
						addChild(D);
					}
				
					min_clicked.x=480;
					min_clicked.y=242;
					removeChild(min);					
					addChild(min_clicked);
					addChild(title);
					childMin_clicked=true;
				
					
					hint_msg.text="DO MULTIPLICATION / DIVISION FIRST";
					addChild(hint_msg);
					addChild(D);
					childDAdded=true;
				
					if(sad_girlAdded==false){
						addChild(sad_girl);
						sad_girlAdded=true;
					}
					break;
				
									
				case div:
				
					
					for(var j:int=0;j<sample1.length;j++){
						removeChildAt(0);
						sample1[j].removeEventListener( MouseEvent.CLICK, click1 );
						}				
					
					sample2=[equals,six,plus,two,min,seven];	
					for(var i:int=0;i<sample2.length;i++){
					sample2[i].x=120+i*60;
					sample2[i].y=330;
					addChild(sample2[i]);
					sample2[i].addEventListener( MouseEvent.CLICK, click2 );
					
							}
					sample1_image.x=190;
					sample1_image.y=240;
					addChild(sample1_image);
					clicks++;
							
					if(childDAdded==true ){
					removeChild(D);
					removeChild(hint_msg);
					removeChild(sad_girl);
							}
				
					check_mark.x=980;
					check_mark.y=220;
					addChild(check_mark);
					addChild(title);
						
					
					}
					
				}
				
				//6+2-7
				
				function click2(event:MouseEvent):void{
					if(sad_girlAdded==true){
						removeChild(sad_girl);
					}
					
				switch(event.currentTarget){
					
				case min:
					noSound.play();
					clicks++;
					min_clicked.x=360;
					min_clicked.y=330;
					addChild(min_clicked);
					removeChild(min);
					hint_msg.text="DO OPERATORS IN \nLEFT TO RIGHT DIRECTION";
					addChild(hint_msg);
					addChild(A);
					childAAdded=true;
					addChild(sad_girl);
				addChild(title);
					break;				
				
					case plus:	
						
					if(childAAdded==true){
					removeChild(A);
					removeChild(hint_msg);
					removeChild(sad_girl);
					removeChild(min_clicked);
						}
					
						
					for(var j:int=0;j<sample2.length;j++){
					removeChildAt(0);
					sample2[j].removeEventListener( MouseEvent.CLICK, click2 );
							
						}			
			
					sample3=[equals,eight,min,seven];	
					for(var i:int=0;i<sample3.length;i++){
					sample3[i].x=120+i*60;
					sample3[i].y=424;
					addChild(sample3[i]);
					sample3[i].addEventListener( MouseEvent.CLICK, click3 );
					
							}
					sample2_image.x=190;
					sample2_image.y=336;
					addChild(sample2_image);
							addChild(sample1_image);
					clicks++;
							
					check_mark.y=320;
					addChild(check_mark);
							addChild(title);
					break;
						
						
						}
						
						
					}
					
					function click3(event:MouseEvent):void{
					clapSound.play();
					clicks++;
						
					sample3_image.x=140;
					sample3_image.y=428;
					addChild(sample3_image);
					addChild(sample1_image);
					check_mark.y=420;
					addChild(check_mark);
						
					well.x=818;
					well.y=270;
					addChild(well);
					well.gotoAndPlay(1);
	
				
						
						addChild(title);	
						
						
						if(clicks>3){
							
							clicks_done.text="YOU DID IT IN  "+clicks+"  CLICKS\t  TRY TO DO IT IN  3";
							}
							else{
							clicks_done.text="YOU DID IT WITHOUT MISTAKES!";
							}
							addChild(clicks_done);
						
						
						for(var j:int=0;j<sample3.length;j++){
						removeChildAt(0);
						sample3[j].removeEventListener( MouseEvent.CLICK, click3 );
						}	
						removeChild(check_mark);
						
						addButtons();
							
					}
					private function addButtons():void{
				
						
						var playButton:More_pencil_button= new More_pencil_button;
						
						addChild(playButton); 
					
						
						playButton.toggle =true; 
						playButton.width=320;
						playButton.height=200;
						playButton.x=240;
						playButton.y=900;
						playButton.addEventListener(MouseEvent.CLICK, onPlay_screen2); 
						
						var exitButton:Exit_pencil = new Exit_pencil; 
						addChild(exitButton); 
					
						
						exitButton.toggle =true; 
						exitButton.width=320;
						exitButton.height=200;
						exitButton.x=880;
						exitButton.y=900;
						exitButton.addEventListener(MouseEvent.CLICK, exit); 
						
				}
				
					private function onPlay_screen2(event:Event):void
				{
					clickButton.play();
					mathGame.changeState(Game.PLAY2_STATE);
				}
					private function exit(event:Event):void
				{
					clickButton.play();
					fscommand("quit");
				}
							
							
							
						
					
			
		private function init(event:Event):void
		{
			stage.scaleMode = StageScaleMode.EXACT_FIT;
			background = new Background(1);
			stage.addChildAt(background,0);
			
			
					
						}
		
	
		
				
		
		private function onMenu(event:Event):void
		{
			mathGame.changeState(Game.MENU_STATE);
		}
		public function update():void
		{
			background.update();
			
		}
		
		public function destroy():void
		{
				
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}
