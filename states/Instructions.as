﻿package states
{
	import Game;
	
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	import fl.transitions.TweenEvent;
	
	import flash.events.Event;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	import fl.controls.Button;
	import objects.*;
	
		import flash.media.Sound;

	import interfaces.IState;
	
	public class Instructions extends Sprite implements IState
	{
		public var game:Game;
		var bgrImage:Intstruction_bgr=new Intstruction_bgr;
		var clickButton:Sound=new click();
		
		
		public function Instructions(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			
			
			bgrImage.x=0;
			bgrImage.y=0;
			addChild(bgrImage);
			
			
			
			var menuButton:Menu_pencil = new Menu_pencil; 
			addChild(menuButton); 
			 
			menuButton.toggle =true;  
			menuButton.width=300;
			menuButton.height=180;
			menuButton.x=900;
			menuButton.y=880;	
			menuButton.addEventListener(MouseEvent.CLICK, onMenu); 
			
			var playButton:Play_pencil=new Play_pencil;			
			addChild(playButton); 
		
			
			playButton.toggle =true; 
			playButton.width=300;
			playButton.height=180;
			playButton.x=400;
			playButton.y=880;
			playButton.addEventListener(MouseEvent.CLICK, onPlay); 
		}

		private function onPlay(event:Event):void
		{
			clickButton.play();
			game.changeState(Game.PLAY_STATE);
		}
		
		private function onMenu(event:Event):void
		{
			clickButton.play();
			game.changeState(Game.MENU_STATE);
		}
		
		public function update():void
		{
			//background.update();
		}
		
		public function destroy():void
		{
			removeFromParent();
		}
		
		private function removeFromParent()
		{
			
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}