﻿package objects
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	
	
	public class Title extends Sprite
	{
		private var titleText:TextField;
		
		public function Title()
		{
			
			var titleFormat:TextFormat = new TextFormat();
			titleFormat.font="Mistral";
			titleFormat.size = 100;
			titleFormat.bold=true;
			

			titleText=new TextField();
			titleText.defaultTextFormat = titleFormat;
			titleText.text = "Which operator you will go for first?";
			addChild(titleText);

			 titleText.width = 1200;
			 titleText.height = 120;
			 titleText.x = 180;
			 titleText.y = 106;
			 titleText.textColor=0xFFFFFF;
			

			
			
			
		}
		
		
	
		
		public function setTitle(title:String):void
		{
			titleText.text = title;
		}
	}
}