﻿package objects
{
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	
	
	public class Background extends Sprite
	{
		private var sheet:Sheet;
		private var blueSheet:BlueSheet;
		
		public function Background(x:int=NaN):void
		{
			if(x==1){		
			sheet = new Sheet();
			sheet.x=0;
			sheet.y=0;
			sheet.width=1920;
			sheet.height=1080;
			addChild(sheet);
			}
			 if(x==2){
			blueSheet = new BlueSheet();
			blueSheet.x=0;
			blueSheet.y=0;
			blueSheet.width=1920;
			blueSheet.height=1080;
			addChild(blueSheet);
	
}
						
		}
			
		
		
		
		public function update():void
		{
			
		}
	public function removeFromParent()
		{
			var child:DisplayObject = this as DisplayObject;
			var parent:DisplayObjectContainer = child.parent;

			parent.removeChild(child);
		}
	}
}